package edu.upenn.cis.cis455.crawler;

import edu.upenn.cis.cis455.crawler.utils.Robotstxtparsed;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.crawler.utils.Worker;
import edu.upenn.cis.cis455.storage.Storage;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.HaltException;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


public class Crawler implements CrawlMaster {
    ///// TODO: you'll need to flesh all of this out. You'll need to build a thread
    static final Logger logger = LogManager.getLogger(CrawlMaster.class);
    // pool of CrawlerWorkers etc.

    public StorageInterface db;
    public int maxSize;
    public URLInfo URLInfo;
    private final AtomicInteger count =  new AtomicInteger(0) ;
//    public List<String> disAllowedPages;
    public static List<Thread> workers;
    public BlockingDeque<String> urls;

    public Map<String, Robotstxtparsed> robotstxtMap = new ConcurrentHashMap<>();
    public LinkedList<String> namedUserAgents = new LinkedList<String>();

    public Set<String> addedURL;
    public final AtomicInteger waitingWorker = new AtomicInteger(0);

    static final int NUM_WORKERS = 10;

    public Crawler(String startUrl, StorageInterface db, int size, int count) {
        // TODO: initialize
//        this.URLInfo = new URLInfo(startUrl);
        this.db = (Storage)db;
        this.maxSize = size;
        this.count.set(count);
        this.addedURL = new HashSet<>();

        workers = new ArrayList<>();

        for (int curr = 0; curr < NUM_WORKERS; curr++) {
            workers.add(new Thread(new Worker(this),String.format("Worker#%d",curr)));
        }

        urls.addLast(startUrl);
        addedURL.add(startUrl);


    }

//    public Crawler(String startUrl, StorageInterface db, int size) {
//        // TODO: initialize
//        URLInfo = new URLInfo(startUrl);
//        db = db;
//        maxSize = size;
//
//    }

    /**
     * Main thread
     */


    // 1. check url:
    // 1.1 get robotTxt -> if not -> new robot txt from url "/robots.txt" -> add to map
    // 1.2 disallowed? crawDelay?

    //  TODO: should I put this robotcompliance in worker?


    public void start() throws IOException, HaltException, InterruptedException {


        // 1.2 disallowed? crawDelay?

        for (Thread workerThread: workers) {
                workerThread.start();
        }

        while(true) {
            try {
                Thread.sleep(TimeUnit.SECONDS.toMillis(10));
                logger.info("Number of workers waiting:" + waitingWorker.get());
                if (waitingWorker.get() == NUM_WORKERS) {
                    Thread.sleep(TimeUnit.SECONDS.toMillis(5));
                    if(waitingWorker.get() == NUM_WORKERS) {
                        break;
                    }
                }
            } catch (InterruptedException e) {
                logger.error("Interrupted while sleeping", e);
            }

        }

//        for(Thread workerThread : workers) {
//            workerThread.interrupt();
//        }

        logger.info("Crawler Done");




    }

    /**
     * We've indexed another document
     */
    @Override
    public void incCount() {
        count.getAndDecrement();
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {
        return count.get() <= 0;
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {
        waitingWorker.getAndDecrement();
        logger.info("Processing an URL");
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {
        waitingWorker.getAndIncrement();
        logger.info("Done!");
    }

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     */
    public static void main(String args[]) throws IOException, InterruptedException {
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;

        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);

        Crawler crawler = new Crawler(startUrl, db, size, count);

        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        crawler.start();

        while (!crawler.isDone())
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        // TODO: final shutdown

        System.out.println("Done crawling!");
    }

}
