package edu.upenn.cis.cis455.storage;

public class StorageFactory {

    public static Storage db;

    public static StorageInterface getDatabaseInstance(String directory) {
        // TODO: factory object, instantiate your storage server

        db  = new Storage(directory);
        return db;
    }
}
